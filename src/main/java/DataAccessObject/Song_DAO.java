package DataAccessObject;

import massignment3.entities.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Song_DAO {

    private final EntityManagerFactory emf;

    public Song_DAO() {

        emf = Persistence.createEntityManagerFactory("massignment3");
    }

    public int checkExistence(int SongId) {

        EntityManager em = emf.createEntityManager();

        Song n = new Song();
        n = em.find(Song.class, SongId);

        if(n == null) {
            em.close();
            return -1;
        }
        else
            em.close();
        return 1;
    }

    public Song find(int Id) {

        EntityManager em = emf.createEntityManager();
        Song c = em.find(Song.class, Id);
        em.close();

        return c;
    }


    public int addSong(Song song, DjSet djset) {
        EntityManager em = emf.createEntityManager();
        djset.getSong().add(song);
        em.getTransaction().begin();
        em.merge(djset);
        em.getTransaction().commit();
        em.close();

        return 1;
    }

    public int updateTitle(int id, String newName) {

        if (checkExistence(id) == -1)
            return -1;
        else
        {
            EntityManager em = emf.createEntityManager();

            Song song = new Song();
            song = em.find(Song.class, id);

            em.getTransaction().begin();
            song.setTitle(newName);
            System.out.println("Song after updation :- " + song);
            em.getTransaction().commit();
            em.close();
            return 1;
        }

    }

    public int updateAuthor(int id, String newName) {

        if (checkExistence(id) == -1)
            return -1;
        else
        {
            EntityManager em = emf.createEntityManager();

            Song song = new Song();
            song = em.find(Song.class, id);

            em.getTransaction().begin();
            song.setAutohr(newName);
            System.out.println("Song after updation :- " + song);
            em.getTransaction().commit();
            em.close();
            return 1;
        }

    }

    public List<Song> listSong()
    {
        EntityManager em = emf.createEntityManager();
        List<Song> ret = em.createNamedQuery("Song.findAll", Song.class).getResultList();

        System.out.println("Song nella tabella: \n" + ret);

        em.close();
        return ret;
    }


    public int removeSong(Song c)
    {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        if (!em.contains(c)) {
            c = em.merge(c);
        }
        em.remove(c);
        em.getTransaction().commit();
        em.close();
        //emf.close();
        return 1;

    }
}
