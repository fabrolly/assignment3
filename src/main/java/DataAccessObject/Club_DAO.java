package DataAccessObject;

import massignment3.entities.Club;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class Club_DAO {

    private final EntityManagerFactory emf;



    public Club_DAO() {
        emf = Persistence.createEntityManagerFactory("massignment3");
    }

    public int checkExistence(int id) {

        EntityManager em = emf.createEntityManager();


        Club n = new Club();
        n = em.find(Club.class, id);

        if(n == null) {
            em.close();
            return -1;
        }
        else
            em.close();
        return 1;
    }

    public int addClub(Club club) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(club);
        em.getTransaction().commit();
        em.close();
        return 1;
    }

    public Club find(int Id) {

        EntityManager em = emf.createEntityManager();
        Club c = em.find(Club.class, Id);
        em.close();

        return c;
    }

    public void close() {
        emf.close();
    }


    public List<Club> listClub()
    {
        EntityManager em = emf.createEntityManager();
        List<Club> ret = em.createNamedQuery("Club.findAll", Club.class).getResultList();


        System.out.println("Club nella tabella: \n" + ret);

        return ret;
    }

    public int updateName(int id, String newName) {
        if (checkExistence(id) == -1)
            return -1;
        else
        {
            EntityManager em = emf.createEntityManager();

            Club club = new Club();
            club = em.find(Club.class, id);

            em.getTransaction().begin();
            club.setName(newName);
            System.out.println("Club after updation :- " + club);
            em.getTransaction().commit();
            return 1;
        }

    }

    public int updateCity(int id, String newCity) {

        if (checkExistence(id) == -1)
            return -1;
        else
        {

            EntityManager em = emf.createEntityManager();

            Club club = new Club();
            club = em.find(Club.class, id);

            em.getTransaction().begin();
            club.setCity(newCity);
            System.out.println("Club after updation :- " + club);
            em.getTransaction().commit();
            return 1;
        }

    }

    public int updateId(int id, int newId) {

        if (checkExistence(id) == -1)
            return -1;
        else
        {

            EntityManager em = emf.createEntityManager();

            Club club = new Club();
            club = em.find(Club.class, id);

            em.getTransaction().begin();
            club.setId(newId);
            System.out.println("Club after updation :- " + club);
            em.getTransaction().commit();
            return 1;
        }

    }

    public int removeClub(Club c)
    {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        if (!em.contains(c)) {
            c = em.merge(c);
        }
        em.remove(c);
        em.getTransaction().commit();
        em.close();
        return 1;

    }
}
