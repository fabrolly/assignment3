package DataAccessObject;

import massignment3.entities.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class DjSet_DAO {

    private final EntityManagerFactory emf;

    public DjSet_DAO() {

        emf = Persistence.createEntityManagerFactory("massignment3");
    }

    public int checkExistence(int DjSetId) {

        EntityManager em = emf.createEntityManager();


        DjSet n = new DjSet();
        n = em.find(DjSet.class, DjSetId);

        if(n == null) {
            em.close();
            return -1;
        }
        else
            em.close();
        return 1;
    }

    public int addDjSet(DjSet djSet) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(djSet);
        em.getTransaction().commit();
        em.close();

        return 1;
    }
    public DjSet find(int Id) {

        EntityManager em = emf.createEntityManager();
        DjSet c = em.find(DjSet.class, Id);
        em.close();

        return c;
    }



    public int updateMusicType(int id, String newName) {

        if (checkExistence(id) == -1)
            return -1;
        else
        {
            EntityManager em = emf.createEntityManager();

            DjSet djset = new DjSet();
            djset = em.find(DjSet.class, id);

            em.getTransaction().begin();
            djset.setMusicType(newName);
            System.out.println("DjSet after updation :- " + djset);
            em.getTransaction().commit();
            return 1;
        }

    }

    public int updateDuration(int id, int newDuration) {

        if (checkExistence(id) == -1)
            return -1;
        else
        {
            EntityManager em = emf.createEntityManager();

            DjSet djset = new DjSet();
            djset = em.find(DjSet.class, id);

            em.getTransaction().begin();
            djset.setDuration(newDuration);
            System.out.println("DjSet after updation :- " + djset);
            em.getTransaction().commit();
            return 1;
        }

    }

    public int updateId(int id, int newName) {

        if (checkExistence(id) == -1)
            return -1;
        else
        {
            EntityManager em = emf.createEntityManager();

            DjSet djset = new DjSet();
            djset = em.find(DjSet.class, id);

            em.getTransaction().begin();
            djset.setId(newName);
            System.out.println("DjSet after updation :- " + djset);
            em.getTransaction().commit();
            return 1;
        }

    }

    public List<DjSet> listDjSet()
    {
        EntityManager em = emf.createEntityManager();
        List<DjSet> ret = em.createNamedQuery("DjSet.findAll", DjSet.class).getResultList();


        System.out.println("DjSet nella tabella: \n" + ret);

        return ret;
    }

    public int remove(DjSet c)
    {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        if (!em.contains(c)) {
            c = em.merge(c);
        }
        em.remove(c);
        em.getTransaction().commit();
        em.close();
        return 1;

    }



}
