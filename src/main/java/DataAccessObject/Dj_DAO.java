package DataAccessObject;

import massignment3.entities.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Dj_DAO {

    private final EntityManagerFactory emf;

    public Dj_DAO() {

        emf = Persistence.createEntityManagerFactory("massignment3");
    }

    public int checkExistence(String DjId) {

        EntityManager em = emf.createEntityManager();


        Dj n = new Dj();
        n = em.find(Dj.class, DjId);

        if(n == null) {
            em.close();
            return -1;
        }
        else
            em.close();
        return 1;
    }

    public int addDj(Dj dj) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(dj);
        em.getTransaction().commit();
        em.close();
        return 1;
    }

    public Dj find(String phone) {

        EntityManager em = emf.createEntityManager();
        Dj c = em.find(Dj.class, phone);
        em.close();

        return c;
    }

    public int addDjPartner(Dj dj1, Dj dj2) {
        EntityManager em = emf.createEntityManager();
        dj1.getDjPartner().add(dj2);
        em.getTransaction().begin();
        em.merge(dj1);
        em.getTransaction().commit();
        em.close();
        return 1;
    }


    public int updateFirstName(String id, String newName) {

        if (checkExistence(id) == -1)
            return -1;
        else
        {
            EntityManager em = emf.createEntityManager();

            Dj dj = new Dj();
            dj = em.find(Dj.class, id);

            em.getTransaction().begin();
            dj.setFirstname(newName);
            System.out.println("Dj after updation :- " + dj);
            em.getTransaction().commit();
            return 1;
        }

    }

    public int updateLastName(String id, String newName) {

        if (checkExistence(id) == -1)
            return -1;
        else
        {
            EntityManager em = emf.createEntityManager();

            Dj dj = new Dj();
            dj = em.find(Dj.class, id);

            em.getTransaction().begin();
            dj.setLastname(newName);
            System.out.println("Dj after updation :- " + dj);
            em.getTransaction().commit();
            return 1;
        }

    }

    public int updateStageName(String id, String newName) {

        if (checkExistence(id) == -1)
            return -1;
        else
        {
            EntityManager em = emf.createEntityManager();

            Dj dj = new Dj();
            dj = em.find(Dj.class, id);

            em.getTransaction().begin();
            dj.setStageName(newName);
            System.out.println("Dj after updation :- " + dj);
            em.getTransaction().commit();
            return 1;
        }

    }

    public int updatePhone(String id, String newPhone) {

        if (checkExistence(id) == -1)
            return -1;
        else
        {
            EntityManager em = emf.createEntityManager();

            Dj dj = new Dj();
            dj = em.find(Dj.class, id);

            em.getTransaction().begin();
            dj.setPhoneNumber(newPhone);
            System.out.println("Dj after updation :- " + dj);
            em.getTransaction().commit();
            return 1;
        }

    }

    public List<Dj> listDj()
    {
        EntityManager em = emf.createEntityManager();
        List<Dj> ret = em.createNamedQuery("Dj.findAll", Dj.class).getResultList();


        System.out.println("Dj nella tabella: \n" + ret);

        return ret;
    }


    public int remove(Dj c)
    {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        if (!em.contains(c)) {
            c = em.merge(c);
        }
        em.remove(c);
        em.getTransaction().commit();
        em.close();
        return 1;

    }
}
