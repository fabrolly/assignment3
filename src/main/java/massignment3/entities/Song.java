package massignment3.entities;


import java.io.Serializable;
import javax.persistence.*;

@NamedQueries({
        @NamedQuery(name = "Song.findAll", query = "SELECT u FROM Song u")
})

@Entity
public class Song implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)


    private int id;


    private String author;

    private String title;


    public Song() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int idDjNew) {
        this.id = idDjNew;
    }

    public String getAutohr() {
        return this.author;
    }

    public void setAutohr(String autohr) {
        this.author = autohr;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Song [id=" + id + ", author=" + author
                + ", title=" + title + "  ]";
    }

}