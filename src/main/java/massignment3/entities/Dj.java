package massignment3.entities;

import org.eclipse.persistence.annotations.CascadeOnDelete;

import java.io.Serializable;
import javax.persistence.*;
import java.util.*;

@NamedQueries({
    @NamedQuery(name = "Dj.findAll", query = "SELECT u FROM Dj u")
})

@Entity
public class Dj implements Serializable {

	@Id
	private String phoneNumber;


	private String firstname;

	private String lastname;

	private String stageName;

	@ManyToMany
	@JoinTable(name = "PLAY_WITH", joinColumns = @JoinColumn(name = "DJ1_id"), inverseJoinColumns = @JoinColumn(name = "DJ2_id"))
	@CascadeOnDelete
	private Set<Dj> dj_Partner;

	@ManyToOne
	private Club club;

	@ManyToOne
	private DjSet djset;

	//@ManyToMany(cascade = CascadeType.ALL)
	//private Club club;

	 //private DjSet djSet = new DjSet();


	public Dj() {
		dj_Partner= new HashSet<Dj>();
		Club club=new Club();
		DjSet djset = new DjSet();

	}

	public Set<Dj> getDjPartner(){
		return dj_Partner;
	}



	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumberNew) {
		this.phoneNumber = phoneNumberNew;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public String getStageName() {
		return this.stageName;
	}

	public void setStageName(String stageNameNew) {
		this.stageName = stageNameNew;
	}

	public Club getClub() {
		return this.club;
	}

	public void setClub(Club c) {
		this.club = c;
	}

	public DjSet getDjSet() {
		return this.djset;
	}

	public void setDjSet(DjSet c) {
		this.djset = c;
	}

	@Override
	public String toString() {
		return "DJ [ phone=" + phoneNumber
				+ ", firstname=" + firstname + ", lastname=" + lastname + ", lastname=" + lastname + "  ]";
	}

}