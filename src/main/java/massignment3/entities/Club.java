package massignment3.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.*;

@NamedQueries({
        @NamedQuery(name = "Club.findAll", query = "SELECT u FROM Club u")
})

@Entity
public class Club implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)

    private int id;

    private String name;

    private String city;




    public Club() {
        /*dj= new ArrayList<Dj>();*/
    }

    /*public List<Dj> getDj(){
        return dj;
    }*/

    public int getId() {
        return this.id;
    }

    public void setId(int idDjNew) {
        this.id = idDjNew;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*public void setDj(List<Dj> dj) {
        this.dj = dj;
    }*/


    @Override
    public String toString() {
        return "Club [id=" + id + ", city=" + city
                + ", name=" + name + "]";
    }

}