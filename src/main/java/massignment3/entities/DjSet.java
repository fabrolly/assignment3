package massignment3.entities;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.*;
import javax.persistence.*;
import org.eclipse.persistence.annotations.CascadeOnDelete;


@NamedQueries({
        @NamedQuery(name = "DjSet.findAll", query = "SELECT u FROM DjSet u")
})

@Entity
public class DjSet implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)

    private int id;

    private int duration;

    private String musicType;

    @ManyToMany
    @JoinTable(name = "INSIDE", joinColumns = @JoinColumn(name = "DJ_id"), inverseJoinColumns = @JoinColumn(name = "Song_id"))
    @CascadeOnDelete
    private Set<Song> song;




    public DjSet() {
        song= new HashSet<Song>();
    }

    public int getId() {
        return this.id;
    }

    public void setId(int idDjNew) {
        this.id = idDjNew;
    }

    public int getDuration() {
        return this.duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getMusicType() {
        return this.musicType;
    }

    public void setMusicType(String musicType) {
        this.musicType = musicType;
    }


    public Set<Song> getSong(){
        return song;
    }

    @Override
    public String toString() {
        return "DjSet [id=" + id + ", duration=" + duration
                + ", musicType=" + musicType + "  ]";
    }

}