package Services;

import DataAccessObject.*;
import massignment3.entities.*;

public class Dj_Service {

    Dj_DAO Dj_DAO = new Dj_DAO();


    public  int addDj(String FirstName, String LastName, String StagtName, String Phone, Club club, DjSet djset) {

        Dj c = new Dj();

        if(FirstName.equals("") || LastName.equals("") || StagtName.equals("") || Phone.equals("") ) {
            //System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        c.setStageName(StagtName);
        c.setFirstname(FirstName);
        c.setLastname(LastName);
        c.setPhoneNumber(Phone);
        c.setClub(club);
        c.setDjSet(djset);

        if(Dj_DAO.addDj(c) == 1) {
            //System.out.println("Done!");
            return 1;
        }
        else {
            //System.out.println("Something went wrong during creation process, see log for more details");
            return -3;
        }
    }

    public Dj find(String phone) {

        Dj c = Dj_DAO.find(phone);
        return c;
    }

    public  int addDjPartner(Dj dj1, Dj dj2) {



        if(Dj_DAO.addDjPartner(dj1, dj2) == 1) {
            //System.out.println("Done!");
            return 1;
        }
        else {
            //System.out.println("Something went wrong during creation process, see log for more details");
            return -3;
        }
    }



    public int updateFirstName(String phone, String nName) {

        if(nName.equals("")) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(nName.length()==0) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(Dj_DAO.find(phone) == null) {
            System.out.println("The entity is not in the database");
            return -2;
        }

        if(Dj_DAO.updateFirstName(phone, nName) == 1) {
            System.out.println("Done!");
            return 1;
        }
        else {
            System.out.println("Something went wrong during update name process, see log for more details");
            return -3;
        }
    }

    public int updateLastName(String phone, String nName) {

        if(nName.equals("")) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(nName.length()==0) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(Dj_DAO.find(phone) == null) {
            System.out.println("The entity is not in the database");
            return -2;
        }

        if(Dj_DAO.updateLastName(phone, nName) == 1) {
            System.out.println("Done!");
            return 1;
        }
        else {
            System.out.println("Something went wrong during update name process, see log for more details");
            return -3;
        }
    }

    public int updateStageName(String phone, String nName) {

        if(nName.equals("")) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(nName.length()==0) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(Dj_DAO.find(phone) == null) {
            System.out.println("The entity is not in the database");
            return -2;
        }

        if(Dj_DAO.updateStageName(phone, nName) == 1) {
            System.out.println("Done!");
            return 1;
        }
        else {
            System.out.println("Something went wrong during update name process, see log for more details");
            return -3;
        }
    }


    public int updatePhone(String phone, String nPhone) {

        if(nPhone.equals("")) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(nPhone.length()==0) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(Dj_DAO.find(phone) == null) {
            System.out.println("The entity is not in the database");
            return -2;
        }

        if(Dj_DAO.updatePhone(phone, nPhone) == 1) {
            System.out.println("Done!");
            return 1;
        }
        else {
            System.out.println("Something went wrong during update name process, see log for more details");
            return -3;
        }
    }

    public int remove(String phone) {

        if(find(phone) == null) {
            return -1;
        }
        Dj c=find(phone);

        if(Dj_DAO.remove(c) == 1) {
            return 1;
        }
        else {
            return -3;
        }
    }

}
