package Services;

import DataAccessObject.Club_DAO;
import massignment3.entities.*;

public class Club_Service {

    Club_DAO Club_DAO = new Club_DAO();


    public  int addClub(String Name, String City) {

        Club c = new Club();

        if(Name.equals("")) {
            //System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(City.equals("")) {
            //System.out.println("Nationality field is empty, insert a non-empty name");
            return -2;
        }

        c.setName(Name);
        c.setCity(City);

        if(Club_DAO.addClub(c) == 1) {
            //System.out.println("Done!");
            return 1;
        }
        else {
            //System.out.println("Something went wrong during creation process, see log for more details");
            return -3;
        }
    }

    public Club find(int Id) {

        Club c = Club_DAO.find(Id);
        return c;
    }


    public int updateName(int Id, String nName) {

        if(nName.equals("")) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(nName.length()==0) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(Club_DAO.find(Id) == null) {
            System.out.println("The entity is not in the database");
            return -2;
        }

        if(Club_DAO.updateName(Id, nName) == 1) {
            System.out.println("Done!");
            return 1;
        }
        else {
            System.out.println("Something went wrong during update name process, see log for more details");
            return -3;
        }
    }


    public  int updateCity(int Id, String nCity) {

        if(nCity.equals("")) {
            System.out.println("Nationality field is empty, insert a non-empty name");
            return -1;
        }

        if(nCity.length()==0) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(Club_DAO.find(Id) == null) {
            System.out.println("The entity is not in the database");
            return -2;
        }

        if(Club_DAO.updateCity(Id, nCity) == 1) {
            System.out.println("Done!");
            return 1;
        }
        else {
            System.out.println("Something went wrong during update name process, see log for more details");
            return -3;
        }
    }

    public int remove(int Id) {

        if(find(Id) == null) {
            //System.out.println("The entity is not in the database");
            return -1;
        }
        Club c=find(Id);

        if(Club_DAO.removeClub(c) == 1) {
            //System.out.println("Done!");
            return 1;
        }
        else {
            //System.out.println("Something went wrong during delete process, see log for more details");
            return -3;
        }
    }

}
