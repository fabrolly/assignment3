package Services;

import DataAccessObject.*;
import massignment3.entities.*;

public class Song_Service {

    Song_DAO Song_DAO = new Song_DAO();


    public  int addSong(String Title, String Author, DjSet d) {

        Song c = new Song();

        if(Title.equals("") || Author.equals("")) {
            //System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        c.setAutohr(Author);
        c.setTitle(Title);

        if(Song_DAO.addSong(c, d) == 1) {
            //System.out.println("Done!");
            return 1;
        }
        else {
            //System.out.println("Something went wrong during creation process, see log for more details");
            return -3;
        }
    }

    public Song find(int Id) {

        Song c = Song_DAO.find(Id);
        return c;
    }


    public int updateTitle(int Id, String nName) {

        if(nName.equals("")) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(nName.length()==0) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(Song_DAO.find(Id) == null) {
            System.out.println("The entity is not in the database");
            return -2;
        }

        if(Song_DAO.updateTitle(Id, nName) == 1) {
            System.out.println("Done!");
            return 1;
        }
        else {
            System.out.println("Something went wrong during update name process, see log for more details");
            return -3;
        }
    }

    public int updateAuthor(int Id, String nName) {

        if(nName.equals("")) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(nName.length()==0) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(Song_DAO.find(Id) == null) {
            System.out.println("The entity is not in the database");
            return -2;
        }

        if(Song_DAO.updateAuthor(Id, nName) == 1) {
            System.out.println("Done!");
            return 1;
        }
        else {
            System.out.println("Something went wrong during update name process, see log for more details");
            return -3;
        }
    }



    public int remove(int Id) {

        if(find(Id) == null) {
            return -1;
        }
        Song s=find(Id);

        if(Song_DAO.removeSong(s) == 1) {
            return 1;
        }
        else {
            return -3;
        }
    }

}
