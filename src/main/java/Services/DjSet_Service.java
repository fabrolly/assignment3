package Services;

import DataAccessObject.DjSet_DAO;
import massignment3.entities.*;

public class DjSet_Service {

    DjSet_DAO DjSet_DAO = new DjSet_DAO();


    public int addDjSet(int Duration, String MusicType) {

        DjSet c = new DjSet();

        if(MusicType.equals("")) {
            return -1;
        }

        c.setMusicType(MusicType);
        c.setDuration(Duration);


        if(DjSet_DAO.addDjSet(c) == 1) {
            return 1;
        }
        else {
            return -3;
        }
    }

    public DjSet find(int Id) {

        DjSet c = DjSet_DAO.find(Id);
        return c;
    }


    public int updateMusicType(int Id, String nName) {

        if(nName.equals("")) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(nName.length()==0) {
            System.out.println("Name field is empty, insert a non-empty name");
            return -1;
        }

        if(DjSet_DAO.find(Id) == null) {
            System.out.println("The entity is not in the database");
            return -2;
        }

        if(DjSet_DAO.updateMusicType(Id, nName) == 1) {
            System.out.println("Done!");
            return 1;
        }
        else {
            System.out.println("Something went wrong during update name process, see log for more details");
            return -3;
        }
    }

    public int updateDuration(int Id, int nDuration) {


        if(nDuration==0) {
            System.out.println("duration is 0, illegal");
            return -1;
        }

        if(DjSet_DAO.find(Id) == null) {
            System.out.println("The entity is not in the database");
            return -2;
        }

        if(DjSet_DAO.updateDuration(Id, nDuration) == 1) {
            System.out.println("Done!");
            return 1;
        }
        else {
            System.out.println("Something went wrong during update name process, see log for more details");
            return -3;
        }
    }

    public int remove(int Id) {

        if(find(Id) == null) {
            return -1;
        }
        DjSet d=find(Id);

        if(DjSet_DAO.remove(d) == 1) {
            return 1;
        }
        else {
            return -3;
        }
    }

}
