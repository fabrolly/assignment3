package massignment3.test;


import Services.DjSet_Service;
import Services.Song_Service;
import DataAccessObject.Song_DAO;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;



@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Song_EntityTest
{
    Song_Service Song_Service =new Song_Service();
    DjSet_Service DjSet_Service = new DjSet_Service();
    Song_DAO Song_DAO = new Song_DAO();


    @Test
    public void Insert()
    {
        int a=0;

        a+= Song_Service.addSong("You block me on Facebook", "scem", DjSet_Service.find(1));
        a+= Song_Service.addSong("You block me on Facebook", "scan", DjSet_Service.find(4));
        a+= Song_Service.addSong("You block me on Facebook", "scan", DjSet_Service.find(5));



        if(a != 3)
            Assert.fail("ERROR durante l'aggiunta di un Djset al DB");

        if(Song_DAO.checkExistence(1) == -1)
            Assert.fail("ERROR nell'inserimento del 1° Djset");

        if(Song_DAO.checkExistence(2) == -1)
            Assert.fail("ERROR nell'inserimento del 2° Djset");

        System.out.println("Insert() PASSED");

    }

    @Test
    public void Search() {


        if(!Song_Service.find(1).getTitle().equals("You block me on Facebook")) {
            Assert.fail("ERROR");
        }

        System.out.println("Search() PASSED");
    }

    @Test
    public void Modify() {

        if(Song_Service.updateAuthor(1, "cemo") != 1) {
            Assert.fail("ERROR: a corret author update was blocked");
        }

        if(Song_Service.updateAuthor(1, "") != -1) {
            Assert.fail("ERROR: a empty author was used to update");
        }

        if(!Song_Service.find(1).getAutohr().equals("cemo")) {
            Assert.fail("ERROR: update author wasn't performed");
        }

        if(Song_Service.updateTitle(2, "You block me on Twitter") != 1) {
            Assert.fail("ERROR: update Title wasn't performed");
        }

        if(Song_Service.updateTitle(2, "") != -1) {
            Assert.fail("ERROR: a empty Title was used to update");
        }

        if(!Song_Service.find(2).getTitle().equals("You block me on Twitter")) {
            Assert.fail("ERROR: update Title wasn't performed");
        }

        System.out.println("Modify_Search() PASSED");
    }

    @Test
    public void Remove() {

        //The return value can be -1 (not exist) or 1 (removed). Because if I execute the test more then one time, the element with this ID can be already removed.
        //The existing checking is in "INSERT" test method, then I don't care to check if the -1 returned value is right or not
        //Then the test fails only if I have -3 has returned value (Which means only if the removal of an object that exists fails, the test failed)
        if(Song_Service.remove(3) == -3) {
            Assert.fail("ERROR: a legal remove was blocked");
        }

            System.out.println("Modify_Search() PASSED");
        }



}
