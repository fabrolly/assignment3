package massignment3.test;


import DataAccessObject.*;
import Services.*;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;



@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DjSet_EntityTest
{
    DjSet_Service DjSet_Service =new DjSet_Service();
    DjSet_DAO DjSet_DAO = new DjSet_DAO();


    @Test
    public void Insert()
    {
        int a=0;

        a+= DjSet_Service.addDjSet(80, "Techno");
        a+= DjSet_Service.addDjSet(75, "House");
        a+= DjSet_Service.addDjSet(90, "Commerciale");
        a+= DjSet_Service.addDjSet(20, "Underground");
        a+= DjSet_Service.addDjSet(20, "Indie");
        a+= DjSet_Service.addDjSet(20, "Pop");


        if(a != 6)
            Assert.fail("ERROR durante l'aggiunta di un Djset al DB");

        if(DjSet_DAO.checkExistence(1) == -1)
            Assert.fail("ERROR nell'inserimento del 1° Djset");

        if(DjSet_DAO.checkExistence(2) == -1)
            Assert.fail("ERROR nell'inserimento del 2° Djset");

        System.out.println("Insert() PASSED");

    }

    @Test
    public void Search() {


        if(!DjSet_Service.find(3).getMusicType().equals("Commerciale")) {
            Assert.fail("ERROR");
        }

        if(!DjSet_Service.find(2).getMusicType().equals("House")) {
            Assert.fail("ERROR");
        }

        if(DjSet_Service.find(3).getDuration()!=90) {
            Assert.fail("ERROR");
        }

        System.out.println("Search() PASSED");
    }

    @Test
    public void Modify() {

        if(DjSet_Service.updateDuration(1, 81) != 1) {
            Assert.fail("ERROR: a corret duration update was blocked");
        }

        if(DjSet_Service.updateDuration(1, 0) != -1) {
            Assert.fail("ERROR: a empty duration (=0) was used to update");
        }

        if(DjSet_Service.find(1).getDuration()!=81) {
            Assert.fail("ERROR: update duration wasn't performed");
        }

        if(DjSet_Service.find(2).getMusicType().equals("Commercialee")) {
            Assert.fail("ERROR: Commercialee not exist");
        }

        if(DjSet_Service.updateMusicType(2, "") != -1) {
            Assert.fail("ERROR: a empty musicType was used to update");
        }

        if(DjSet_Service.updateMusicType(1,"Commerciale")!= 1) {
            Assert.fail("ERROR: update musicType wasn't performed");
        }


        if(DjSet_Service.find(2).getMusicType().equals("Commerciale")) {
            Assert.fail("ERROR: Commercialee  exist");
        }

        System.out.println("Modify_Search() PASSED");
    }

    @Test
    public void Remove() {

        //The return value can be -1 (not exist) or 1 (removed). Because if I execute the test more then one time, the element with this ID can be already removed.
        //The existing checking is in "INSERT" test method, then I don't care to check if the -1 returned value is right or not
        //Then the test fails only if I have -3 has returned value (Which means only if the removal of an object that exists fails, the test failed)
        if(DjSet_Service.remove(6) == -3) {

            Assert.fail("ERROR: a legal remove was blocked");
        }
        System.out.println("Modify_Search() PASSED");
    }


}
