package massignment3.test;

import Services.*;
import DataAccessObject.*;
import org.junit.*;
import org.junit.runners.MethodSorters;



@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Club_EntityTest
{
    Club_DAO Club_DAO = new Club_DAO();
    Club_Service Club_Service =new Club_Service();


    @Test
    public void Insert()
    {
        int a=0;

        a+= Club_Service.addClub("Orsa Maggiore", "Lecco");
        a+= Club_Service.addClub("Club Modà", "Erba");
        a+= Club_Service.addClub("KKLASS", "Tavernerio");
        a+= Club_Service.addClub("MOLTO", "Lissone");

        if(a != 4)
            Assert.fail("ERROR durante l'aggiunta di un Club al DB");

        if(Club_DAO.checkExistence(1) == -1)
            Assert.fail("ERROR nell'inserimento del 1° Club");

        if(Club_DAO.checkExistence(2) == -1)
            Assert.fail("ERROR nell'inserimento del 2° Club");

        if(Club_DAO.checkExistence(3) == -1)
            Assert.fail("ERROR nell'inserimento del 3° Club");



        System.out.println("Insert() PASSED");


    }

    @Test
    public void Search() {


        if(!Club_Service.find(3).getCity().equals("Tavernerio")) {
            Assert.fail("ERROR");
        }

        if(!Club_Service.find(3).getName().equals("KKLASS")) {
            Assert.fail("ERROR");
        }

        System.out.println("Search() PASSED");
    }

    @Test
    public void Modify() {

        if(Club_Service.updateCity(1, "MilanoA") != 1) {
            Assert.fail("ERROR: a corret city update was blocked");
        }

        if(Club_Service.updateCity(1, "") != -1) {
            Assert.fail("ERROR: a empty city was used to update");
        }

        if(!Club_Service.find(1).getCity().equals("MilanoA")) {
            Assert.fail("ERROR: update city wasn't performed");
        }

        if(Club_Service.updateName(2, "2001 Club") != 1) {
            Assert.fail("ERROR: a corret name update was blocked");
        }

        if(Club_Service.updateName(2, "") != -1) {
            Assert.fail("ERROR: a empty name was used to update");
        }

        if(Club_Service.updateName(100, "French") != -2) {
            Assert.fail("ERROR: was performated a no-sense name update");
        }

        if(!Club_Service.find(2).getName().equals("2001 Club")) {
            Assert.fail("ERROR: update name wasn't performed");
        }

        System.out.println("Modify_Search() PASSED");
    }


    @Test
    public void Remove() {

        //The return value can be -1 (not exist) or 1 (removed). Because if I execute the test more then one time, the element with this ID can be already removed.
        //The existing checking is in "INSERT" test method, then I don't care to check if the -1 returned value is right or not
        //Then the test fails only if I have -3 has returned value (Which means only if the removal of an object that exists fails, the test failed)
        if(Club_Service.remove(4) == -3) {
            Assert.fail("ERROR: a legal remove was blocked");
        }


        System.out.println("Modify_Search() PASSED");
    }




}
