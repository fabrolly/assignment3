package massignment3.test;

import DataAccessObject.Dj_DAO;
import Services.Club_Service;
import Services.DjSet_Service;
import Services.Dj_Service;
import org.junit.Assert;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;



@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Dj_EntityTest
{
    Dj_DAO Dj_DAO = new Dj_DAO();

    Dj_Service Dj_Service =new Dj_Service();
    DjSet_Service DjSet_Service =new DjSet_Service();
    Club_Service Club_Service =new Club_Service();


    @Test
    public void Insert()
    {
        int a=0;

        a+= Dj_Service.addDj("Michel", "Vitali", "Malvibra", "1234", Club_Service.find(1), DjSet_Service.find(1));
        a+= Dj_Service.addDj("Alessandro", "Silveri", "SLV", "12345", Club_Service.find(1), DjSet_Service.find(2));
        a+= Dj_Service.addDj("Fabio", "Tavola", "FABTAV", "123456", Club_Service.find(1), DjSet_Service.find(1));

        //self relation test
        a+= Dj_Service.addDjPartner(Dj_Service.find("1234"), Dj_Service.find("12345"));
        a+= Dj_Service.addDjPartner(Dj_Service.find("12345"), Dj_Service.find("123456"));



        if(a != 5)
            Assert.fail("ERROR durante l'aggiunta di un Club al DB");

        if(Dj_DAO.checkExistence("1234") == -1)
            Assert.fail("ERROR nell'inserimento del 1° DJ");

        if(Dj_DAO.checkExistence("12345") == -1)
            Assert.fail("ERROR nell'inserimento del 2° DJ");

        if(Dj_DAO.checkExistence("123456") == -1)
            Assert.fail("ERROR nell'inserimento del 2° DJ");


        System.out.println("Insert() PASSED");

    }

    @Test
    public void Search() {

        if(!Dj_Service.find("123456").getLastname().equals("Tavola")) {
            Assert.fail("ERROR");
        }

        if(!Dj_Service.find("12345").getPhoneNumber().equals("12345")) {
            Assert.fail("ERROR");
        }

        System.out.println("Search() PASSED");
    }

    @Test
    public void Modify() {

        if(Dj_Service.updateFirstName("1234", "Michele") != 1) {
            Assert.fail("ERROR: a corret name update was blocked");
        }

        if(Dj_Service.updateFirstName("1234", "") != -1) {
            Assert.fail("ERROR: a empty Name was used to update");
        }

        if(!Dj_Service.find("1234").getFirstname().equals("Michele")) {
            Assert.fail("ERROR: update Name wasn't performed");
        }

        if(Dj_Service.updateStageName("12345", "SLVR") != 1) {
            Assert.fail("ERROR: a corret stagename update was blocked");
        }

        if(Dj_Service.updateStageName("12345", "") != -1) {
            Assert.fail("ERROR: a empty stagename was used to update");
        }

        if(Dj_Service.updateStageName("123445676549393", "SILLLLVVVERRRR") != -2) {
            Assert.fail("ERROR: was performated a no-sense stagename update");
        }

        if(!Dj_Service.find("12345").getStageName().equals("SLVR")) {
            Assert.fail("ERROR: update stagename wasn't performed");
        }

        System.out.println("Modify_Search() PASSED");
    }

    @Test
    public void Remove() {

        //The return value can be -1 (not exist) or 1 (removed). Because if I execute the test more then one time, the element with this ID can be already removed.
        //The existing checking is in "INSERT" test method, then I don't care to check if the -1 returned value is right or not
        //Then the test fails only if I have -3 has returned value (Which means only if the removal of an object that exists fails, the test failed)

        if(Dj_Service.remove("1234") == -3) {
            Assert.fail("ERROR");
        }

        System.out.println("Modify_Search() PASSED");
    }



}
