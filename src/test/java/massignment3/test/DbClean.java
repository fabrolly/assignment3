package massignment3.test;

import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class DbClean {


    @Test
    public void DBClean(){

        final EntityManagerFactory emf;
        emf = Persistence.createEntityManagerFactory("massignment3");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Query d = em.createNativeQuery("DROP DATABASE IF EXISTS eventi;");
        d.executeUpdate();
        Query c = em.createNativeQuery("CREATE database eventi;");
        c.executeUpdate();
        em.getTransaction().commit();
        em.close();
        emf.close();

    }
}
