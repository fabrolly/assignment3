Fabrizio Olivadese 820864



# Istruzioni per l'esecuzione
1.	Scaricare la Repository.

2.	Importare la cartella scaricata come progetto JPA maven

3.	Installare MySQL e creare un database dal nome "eventi" (# create database eventi; )

4.  Modificare nel file \src\main\META-INF\persistence.xml USERNAME e PASSWORD con i dati del proprio database MYSQL appena installato

5.	Avviare la classe \src\test\FinalTest.java per lanciare i test.



# Relazione
La relazione si trova sotto il nome di "Third_Assigment_Fabrizio_Olivadese_820864.pdf" in questa cartella.